def is_even?(n)
	remainder = n % 2

	if remainder == 0
		return true
	end

	if remainder == 1
		return false
	end
end

puts is_even?(27)